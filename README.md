Authentication plugin for ACM Class MediaWiki
=============================================

Work with the ConfirmAccount extension
--------------------------------------

Download the ConfirmAccount extension from <http://www.mediawiki.org/wiki/Special:ExtensionDistributor/ConfirmAccount>.

Append `LocalSettings.php`.

    require_once("$IP/extensions/ConfirmAccount/ConfirmAccount.php");
    $wgUseRealNamesOnly = false;
    $wgAccountRequestMinWords = 1;
    $wgAccountRequestToS = false;
    $wgAccountRequestExts = array( 'txt', 'pdf', 'text' );
    $wgConfirmAccountContact = 'ADMIN EMAIL ADDRESS HERE';

Change to the MediaWiki directory and run:

    php maintenance/update.php

Ensure `images/` is writable.
Otherwise it will fail on approving requests,
and Log file shows `wfMkdirParents: failed to mkdir` etc. as follows.

    [Tue Jan 01 17:05:34 2013] [error] [client 127.0.0.1] PHP Warning:  wfMkdirParents: failed to mkdir "/var/www/wiki/images/accountcreds/m/my/my_" mode 0777 in /var/www/wiki/includes/GlobalFunctions.php on line 2546, referer: http://localhost/wiki/index.php?title=Special:ConfirmAccounts/authors&acrid=3
    [Tue Jan 01 17:05:34 2013] [error] [client 127.0.0.1] PHP Catchable fatal error:  Argument 1 passed to AccountConfirmSubmission::acceptRequest_rollback() must be an instance of Database, instance of DatabaseMysql given, called in /var/www/wiki/extensions/ConfirmAccount/business/AccountConfirmSubmission.php on line 205 and defined in /var/www/wiki/extensions/ConfirmAccount/business/AccountConfirmSubmission.php on line 333, referer: http://localhost/wiki/index.php?title=Special:ConfirmAccounts/authors&acrid=3

User creation requests are stored locally in the MediaWiki database,
so pending users cannot log into any services.
After approval, the user is created in both MediaWiki and AcmAuthPlugin.

There is a related known issue:

> AuthPlugin stuff: If a central login like CentralAuth is used, 
> when accounts are confirmed and made, we may get name collisions 
> if each wiki of the farm lets you request accounts on it. 
> Collisions are dealt with by picking a new name.


Other useful configuration
--------------------------

    $wgNamespacesWithSubpages[NS_MAIN] = true;
