<?php
require_once("$IP/includes/AuthPlugin.php");

/**
 * Authentication plugin for ACM Class.
 */
class AcmAuthPlugin extends AuthPlugin {

  /**
   * User database configuration.
   */
  protected $conf;

  public function __construct($conf) {
    $this->conf = $conf;

    global $wgHooks;
    $wgHooks['ConfirmEmailComplete'][] = 'AcmAuthPlugin::onConfirmEmailComplete';
    $wgHooks['UserSaveOptions'][] = 'AcmAuthPlugin::onUserSaveOptions';
  }

  public static function onConfirmEmailComplete($user) {
    global $wgAuth;
    $wgAuth->updateExternalDB($user);
    return true;
  }

  public static function onUserSaveOptions($user, &$options) {
    global $wgAuth;
    $wgAuth->updateExternalDB($user);
    return true;
  }

  protected function connectToDB() {
    return new Database($this->conf['host'],
                        $this->conf['username'],
                        $this->conf['password'],
                        $this->conf['database'],
                        FALSE, 0, '');
  }

  /**
   * Check whether there exists a user account with the given name.
   * The name will be normalized to MediaWiki's requirements, so
   * you might need to munge it (for instance, for lowercase initial
   * letters).
   *
   * @param $username String: username.
   * @return bool
   */
  public function userExists($username) {
    $db = $this->connectToDB();
    return FALSE !== $db->selectRow($this->conf['table'],
                                    'name',
                                    array('name' => $this->formatName($username)),
                                    __METHOD__);
  }

  /**
   * Check if a username+password pair is a valid login.
   * The name will be normalized to MediaWiki's requirements, so
   * you might need to munge it (for instance, for lowercase initial
   * letters).
   *
   * @param $username String: username.
   * @param $password String: user password.
   * @return bool
   */
  public function authenticate($username, $password) {
    $db = $this->connectToDB();
    $result = $db->selectRow($this->conf['table'],
                             array('pass', 'salt', 'iter'),
                             array('name' => $this->formatName($username)),
                             __METHOD__);
    return $this->hashPassword($password, $result->salt, $result->iter) === $result->pass;
  }

  protected function hashPassword($password, $salt, $iter) {
    for ($i = 0; $i < $iter; $i++) {
      $password = md5($password . $salt);
    }
    return $password;
  }

  /**
   * When a user logs in, optionally fill in preferences and such.
   * For instance, you might pull the email address or real name from the
   * external user database.
   *
   * The User object is passed by reference so it can be modified; don't
   * forget the & on your function declaration.
   *
   * @param $user User object
   * @return bool
   */
  public function updateUser(&$user) {
    $this->fillUser($user);
    return true;
  }

  protected function fillUser(&$user) {
    $db = $this->connectToDB();
    $result = $db->selectRow($this->conf['table'],
                             array('display_name', 'email'),
                             array('name' => $this->formatName($user->getName())),
                             __METHOD__);
    $user->setRealName($result->display_name);
    $user->setEmail($result->email);
    $user->saveSettings();
  }

  /**
   * Return true if the wiki should create a new local account automatically
   * when asked to login a user who doesn't exist locally but does in the
   * external auth database.
   *
   * If you don't automatically create accounts, you must still create
   * accounts in some way. It's not possible to authenticate without
   * a local account.
   *
   * This is just a question, and shouldn't perform any actions.
   *
   * @return Boolean
   */
  public function autoCreate() {
    return true;
  }

  /**
   * Allow a property change? Properties are the same as preferences
   * and use the same keys. 'Realname' 'Emailaddress' and 'Nickname'
   * all reference this.
   *
   * @param $prop string
   *
   * @return Boolean
   */
  public function allowPropChange($prop) {
    return true;
  }

  /**
   * Can users change their passwords?
   *
   * @return bool
   */
  public function allowPasswordChange() {
    return true;
  }

  /**
   * Should MediaWiki store passwords in its local database?
   *
   * @return bool
   */
  public function allowSetLocalPassword() {
    return false;
  }

  /**
   * Set the given password in the authentication database.
   * As a special case, the password may be set to null to request
   * locking the password to an unusable value, with the expectation
   * that it will be set later through a mail reset or other method.
   *
   * Return true if successful.
   *
   * @param $user User object.
   * @param $password String: password.
   * @return bool
   */
  public function setPassword($user, $password) {
    $iter = rand(2, 16);
    $salt = $this->newSalt();
    $pass = $this->hashPassword($password, $salt, $iter);
    $db = $this->connectToDB();
    $result = $db->update($this->conf['table'],
                          array('pass' => $pass,
                                'salt' => $salt,
                                'iter' => $iter),
                          array('name' => $this->formatName($user->getName())),
                          __METHOD__);
    return $result;
  }
  
  protected function newSalt() {
    return md5(uniqid(time(), TRUE));
  }
  
  /**
   * Update user information in the external authentication database.
   * Return true if successful.
   *
   * This function is not called by MediaWiki by default, so we 
   * register several hooks to invoke this function appropriately.
   *
   * @param $user User object.
   * @return Boolean
   */
  public function updateExternalDB($user) {
    $updates = array();
    if (strlen($user->getRealName()) > 0) {
      $updates['display_name'] = $user->getRealName();
    }
    if (strlen($user->getEmail()) > 0) {
      $updates['email'] = $user->getEmail();
    }
    if (count($updates) <= 0) {
      // skip
      return true;
    }
    
    $db = $this->connectToDB();
    $result = $db->update($this->conf['table'],
                          $updates,
                          array('name' => $this->formatName($user->getName())),
                          __METHOD__);
    return $result;
  }

  /**
   * Check to see if external accounts can be created.
   * Return true if external accounts can be created.
   * @return Boolean
   */
  public function canCreateAccounts() {
    return true;
  }

  /**
   * Add a user to the external authentication database.
   * Return true if successful.
   *
   * Should set $wgEnableEmail to true to enable Email.
   * Create/update time depends on local time zone settings.
   *
   * @param $user User: only the name should be assumed valid at this point
   * @param $password String
   * @param $email String
   * @param $realname String
   * @return Boolean
   */
  public function addUser($user, $password, $email = '', $realname = '') {
    $iter = rand(2, 16);
    $salt = $this->newSalt();
    $pass = $this->hashPassword($password, $salt, $iter);
    $db = $this->connectToDB();
    $result = $db->insert($this->conf['table'],
                          array('name' => $this->formatName($user->getName()),
                                'pass' => $pass,
                                'salt' => $salt,
                                'iter' => $iter,
                                'email' => $email,
                                'display_name' => $realname,
                                'created_at'   => date('Y-m-d H:i:s'),
                                'updated_at'   => date('Y-m-d H:i:s')),
                          __METHOD__);
    return $result;
  }

  /**
   * Return true to prevent logins that don't authenticate here from being
   * checked against the local database's password fields.
   *
   * This is just a question, and shouldn't perform any actions.
   *
   * @return Boolean
   */
  public function strict() {
    return true;
  }

  /**
   * Check if a user should authenticate locally if the global authentication fails.
   * If either this or strict() returns true, local authentication is not used.
   *
   * @param $username String: username.
   * @return Boolean
   */
  public function strictUserAuth($username) {
    return true;
  }

  /**
   * When creating a user account, optionally fill in preferences and such.
   * For instance, you might pull the email address or real name from the
   * external user database.
   *
   * The User object is passed by reference so it can be modified; don't
   * forget the & on your function declaration.
   *
   * @param $user User object.
   * @param $autocreate Boolean: True if user is being autocreated on login
   */
  public function initUser(&$user, $autocreate = false) {
    $this->fillUser($user);
  }

  /**
   * The username is translated by MediaWiki before it is passed to 
   * the function: First letter becomes upper case, underscore '_' 
   * become spaces ' '.
   *
   * @param $username string
   * @return string
   */
  protected function formatName($username) {
    $username = strtolower($username);
    $username = str_replace(' ', '_', $username);
    return $username;
  }
}
